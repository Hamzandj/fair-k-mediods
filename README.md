fair-k-mediods
==============

This script is based on the work of :

Novikov, A., 2019. PyClustering: Data Mining Library. 
Journal of Open Source Software, 4(36), p.1230. 
Available at: http://dx.doi.org/10.21105/joss.01230.

And 

Backurs, Arturs et al. (2019).Scalable fair clustering. 
In :arXiv preprint arXiv :1902.03519.



**License**: GNU General Public License


Dependencies
============

**Required packages**: scipy, numpy.

**Python version**: >=3.5 (32-bit, 64-bit)

**C++ version**: >= 14 (32-bit, 64-bit)

Dataset
=======

The dataset used in this work come from Adult Dataset :

https://archive.ics.uci.edu/ml/datasets/adult






